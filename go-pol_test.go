package main

import (
	"reflect"
	"testing"
)

type testCasesStruct []struct {
	testInput []string
	expected  interface{}
}
type splitCurPairTestCasesStruct []struct {
	testInput string
	expected  []string
}
type polCurPairCheckTestCasesStruct []struct {
	curPair       string
	polCurPairs   curPairsStruct
	twoCurrencies []string
	expected      bool
}
type valUserInType func([]string) interface{}

var curPairInputMatchTests = testCasesStruct{
	{[]string{"1/d"}, false},
	{[]string{"a/d"}, false},
	{[]string{"a"}, false},
	{[]string{""}, false},
	{[]string{"_"}, false},
	{[]string{"/"}, false},
	{[]string{"ETF_#@$"}, false},
	{[]string{"ETF_BTC"}, false},
	{[]string{"asd/sad"}, true},
	{[]string{"123/321"}, false},
	{[]string{"3ta/321"}, false},
	{[]string{"123/y43"}, false},
	{[]string{"abc/abcd"}, true},
	{[]string{"abcd/abc"}, true},
	{[]string{"abcd/abc1"}, true},
	{[]string{"abcd/abc11"}, false},
	{[]string{"abcd1/abc1"}, false},
	{[]string{"abc1/abc1"}, false},
	{[]string{"abc1/abc"}, false},
	{[]string{"abc/abc1"}, true},
	{[]string{"123/321"}, false},
}
var curPairInputFormatTests = testCasesStruct{
	{[]string{"eth/btc"}, "ETH_BTC"},
	{[]string{"LTC/BTC"}, "LTC_BTC"},
	{[]string{"xrp/XLM"}, "XRP_XLM"},
	{[]string{"xrpA/XLM"}, "XRPA_XLM"},
	{[]string{"xrp/XLMa"}, "XRP_XLMA"},
	{[]string{"xrp1/XLM"}, "XRP1_XLM"},
	{[]string{"xrp1/XLMa"}, "XRP1_XLMA"},
}
var splitCurPairTests = splitCurPairTestCasesStruct{
	{"ETH_BTC", []string{"ETH", "BTC"}},
	{"XRP_BTC", []string{"XRP", "BTC"}},
	{"LTC_BTC", []string{"LTC", "BTC"}},
}
var polCurPairCheckTests = polCurPairCheckTestCasesStruct{
	{"BTC_EMC2", polCurrencyPairs, []string{"BTC", "EMC2"}, true},
	{"BTC1_EMC2", polCurrencyPairs, []string{"BTC1", "EMC2"}, false},
	{"BTC_ETH", polCurrencyPairs, []string{"BTC", "ETH"}, true},
	{"ETH_BTC", polCurrencyPairs, []string{"ETH", "BTC"}, false},
	{"XRP_BTC", polCurrencyPairs, []string{"XRP", "BTC"}, false},
	{"LTC_BTC", polCurrencyPairs, []string{"LTC", "BTC"}, false},
}

func TestGoPolSuite(t *testing.T) {
	methodTester(t, validateCurPairInput, curPairInputMatchTests)
	methodTester(t, formatCurPairInput, curPairInputFormatTests)
}

func methodTester(t *testing.T, testingMethod valUserInType, testCases testCasesStruct) {
	t.Helper()
	for _, tt := range testCases {
		actual := testingMethod(tt.testInput)
		if actual != tt.expected {
			t.Errorf("\ntestInput%v(%v), expected: %v, actual: %v\n", testingMethod, tt.testInput, tt.expected, actual)
		}
	}
}
func TestSplitCurPair(t *testing.T) {
	for _, tt := range splitCurPairTests {
		actual := splitCurPair(tt.testInput)
		if reflect.DeepEqual(actual, tt.expected) == false {
			t.Errorf("\nsplitCurPair(%v), expected: %v, actual: %v\n", tt.testInput, tt.expected, actual)
		}
	}
}

func TestPoloniexCurPairCheck(t *testing.T) {
	for _, tt := range polCurPairCheckTests {
		actual := poloniexCurPairCheck(tt.curPair, polCurrencyPairs, tt.twoCurrencies)
		if actual != tt.expected {
			t.Errorf("\nsplitCurPair(%v), expected: %v, actual: %v\n", tt.curPair, tt.expected, actual)
		}
	}
	// poloniexCurPairCheck(curPair string, polCurPairs curPairsStruct, twoCurrencies []string)
}
