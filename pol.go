package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type curPairsStruct []struct {
	currency string
}

type polTradeHistory struct {
	GlobalTradeID int    `json:"globalTradeID"`
	TradeID       int    `json:"tradeID"`
	Date          string `json:"date"`
	TradeType     string `json:"type"`
	Rate          string `json:"rate"`
	Amount        string `json:"amount"`
	Total         string `json:"total"`
}

var polCurrencyPairs = curPairsStruct{
	{"BTC_RADS"}, {"USDT_LTC"}, {"ETH_BCH"}, {"BTC_EXP"}, {"BTC_GNT"}, {"ETH_REP"}, {"BTC_OMNI"}, {"USDT_ZEC"},
	{"USDT_BTC"}, {"BTC_FLO"}, {"BTC_BTS"}, {"BTC_HUC"}, {"BTC_GNO"}, {"BTC_GAME"}, {"BTC_STORJ"}, {"BTC_FCT"},
	{"BTC_BELA"}, {"BTC_PPC"}, {"BTC_BTM"}, {"BTC_ETC"}, {"ETH_GNT"}, {"BTC_OMG"}, {"BTC_XPM"}, {"USDT_NXT"},
	{"BTC_NEOS"}, {"BTC_CLAM"}, {"BTC_SC"}, {"BTC_BLK"}, {"XMR_LTC"}, {"USDT_STR"}, {"BTC_BURST"}, {"BTC_STRAT"},
	{"BTC_NMC"}, {"BTC_AMP"}, {"ETH_OMG"}, {"BTC_ARDR"}, {"BTC_ZEC"}, {"ETH_STEEM"}, {"BTC_NXC"}, {"XMR_ZEC"},
	{"ETH_LSK"}, {"BTC_BCH"}, {"ETH_ZEC"}, {"ETH_GAS"}, {"BTC_VTC"}, {"BTC_XMR"}, {"BTC_GAS"}, {"BTC_POT"},
	{"BTC_NAV"}, {"BTC_XBC"}, {"USDT_BCH"}, {"BTC_STR"}, {"BTC_PINK"}, {"USDT_ETH"}, {"BTC_BTCD"}, {"ETH_GNO"},
	{"BTC_VIA"}, {"XMR_BTCD"}, {"BTC_RIC"}, {"BTC_NXT"}, {"BTC_XVC"}, {"BTC_DGB"}, {"BTC_ZRX"}, {"XMR_BCN"},
	{"XMR_DASH"}, {"USDT_DASH"}, {"BTC_FLDC"}, {"BTC_LBC"}, {"BTC_XEM"}, {"BTC_DASH"}, {"XMR_NXT"}, {"XMR_MAID"},
	{"BTC_DCR"}, {"BTC_REP"}, {"ETH_CVC"}, {"XMR_BLK"}, {"BTC_CVC"}, {"BTC_VRC"}, {"BTC_SBD"}, {"BTC_GRC"},
	{"USDT_XRP"}, {"BTC_PASC"}, {"USDT_REP"}, {"BTC_LTC"}, {"BTC_XRP"}, {"BTC_EMC2"}, {"BTC_XCP"}, {"BTC_DOGE"},
	{"BTC_BCN"}, {"USDT_ETC"}, {"BTC_MAID"}, {"USDT_XMR"}, {"BTC_SYS"}, {"BTC_BCY"}, {"BTC_ETH"}, {"ETH_ZRX"},
	{"BTC_LSK"}, {"BTC_STEEM"}, {"ETH_ETC"},
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Poll Poloniex for a 1-minute rolling moving average currency pair rate.\n",
		"Enter a crypto currency pair (i.e.: \"BTC/ETH\"): ")
	text, _ := reader.ReadString('\n')
	validateCurPairInput([]string{text})
	formattedCurPair := formatCurPairInput([]string{text})
	twoCurrencies := splitCurPair(formattedCurPair.(string))
	poloniexCurPairCheck(formattedCurPair.(string), polCurrencyPairs, twoCurrencies)
	displayMovingAverage(formattedCurPair.(string))
}

func validateCurPairInput(curPairString []string) interface{} {
	matched, err := regexp.MatchString("^[a-zA-Z]{3,4}/[a-zA-Z]{3,4}[0-9]{0,1}$", curPairString[0])
	if err != nil {
		log.Fatalln("Invalid currency pair inputted for %v \n\n", matched, "Error: %v ", err)
	}
	return matched
}

func formatCurPairInput(curPairString []string) interface{} {
	formattedCurPair := strings.Replace(curPairString[0], "/", "_", 1)
	formattedCurPair = strings.TrimSpace(formattedCurPair)
	return strings.ToUpper(formattedCurPair)
}

func splitCurPair(curPair string) []string {
	s := strings.Split(curPair, "_")
	return s
}

func poloniexCurPairCheck(curPair string, polCurPairs curPairsStruct, twoCurrencies []string) bool {
	var regexpString = fmt.Sprintf("%v|%v", twoCurrencies[0], twoCurrencies[1])
	var matchCurrency, err = regexp.Compile(regexpString)
	var suggestCurrencies string
	if err != nil {
		log.Fatal("matchCurrency failed. Error:", err)
	}
	for _, curPairIndex := range polCurPairs {
		if curPairIndex.currency == curPair {
			return true
		} else if matchCurrency.MatchString(curPairIndex.currency) {
			suggestCurrencies += curPairIndex.currency + ", "
		}
	}
	return errorPolCurPairCheck(curPair, &suggestCurrencies)
}

func errorCurPairCheckMsg(curPair string, suggestCurrencies interface{}) bool {
	fmt.Println("\n\nCurrency pair not found for:", curPair, "\n\nDid you mean one of these possible currencies?\n\n", suggestCurrencies)
	return false
}

func errorPolCurPairCheck(curPair string, suggestCurrencies *string) bool {
	if len(*suggestCurrencies) == 0 {
		return errorCurPairCheckMsg(curPair, polCurrencyPairs)
	}
	return errorCurPairCheckMsg(curPair, *suggestCurrencies)
}

func getLastMinUnix() (int64, int64) {
	var timeNow, timeOneMinAgo int64
	timeNow = time.Now().Unix()
	timeOneMinAgo = timeNow - 60
	return timeNow, timeOneMinAgo
}

func unmarshalResp(rawTradeHistoryResp []byte) []polTradeHistory {
	var tradeHistoryResponses []polTradeHistory
	err := json.Unmarshal(rawTradeHistoryResp, &tradeHistoryResponses)
	if err != nil {
		log.Fatalln("Converting Poloniex raw response to type \"polCurrencyPairs\" failed.\n",
			"Error:", err)
	}
	return tradeHistoryResponses
}

func poloniexGetLastMin(curPair string) []byte {
	timeNow, timeOneMinAgo := getLastMinUnix()
	url := fmt.Sprintf("https://poloniex.com/public?command=returnTradeHistory&currencyPair=%v&start=%v&end=%v", curPair, timeOneMinAgo, timeNow)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln("\n\nreturnTradeHistory call to Poloniex failed. \nError:", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	return body
}

func getTradeHistoryRates(lastMinPolData []polTradeHistory) []float64 {
	var lastMinPolRates []float64
	for _, polDataItem := range lastMinPolData {
		f, err := strconv.ParseFloat(polDataItem.Rate, 64)
		if err != nil {
			log.Fatalln("Failed while converting raw Poloniex json rate string to float", err)
		}
		lastMinPolRates = append(lastMinPolRates, f)
	}
	return lastMinPolRates
}

func calcMovingAvgerage(lastMinPolRates []float64, formattedCurPair string) float64 {
	var movingAverage float64
	for _, rateItem := range lastMinPolRates {
		movingAverage += rateItem
	}
	movingAverage = movingAverage / float64(len(lastMinPolRates))
	fmt.Printf("%v moving average: %v\n", formattedCurPair, movingAverage)
	return movingAverage
}

func getNewMovingAverage(formattedCurPair string) {
	polRawResp := poloniexGetLastMin(formattedCurPair)
	lastMinPolTrades := unmarshalResp([]byte(polRawResp))
	lastMinPolRates := getTradeHistoryRates(lastMinPolTrades)
	calcMovingAvgerage(lastMinPolRates, formattedCurPair)
}

func displayMovingAverage(curPair string) {
	timeout := time.After(3 * time.Minute)
	tick := time.Tick(3 * time.Second)
	fmt.Println("\nMoving average for \"", curPair, "\" found. Press Ctrl+C to close.\n",
		"Program will close itself after three minutes of displaying the moving average.")
	for {
		select {
		case <-timeout:
			log.Fatalln("Timed out.")
		case <-tick:
			getNewMovingAverage(curPair)
		}
	}
}
